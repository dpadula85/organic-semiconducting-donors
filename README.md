Geometries
==========
The 249 geometries of the donors are gathered in the donors.xyz file.
They have been obtained via DFT/B3LYP/6-31G* optimisation in Gaussian16.
To split them, run

`obabel donors.xyz -Omol.xyz --separate -m`

This will generate 249 .xyz file named mol_N.xyz

The 3 geometries of the acceptors are gathered in the acceptors.xyz file.
They have been obtained via DFT/B3LYP/6-31G* optimisation in Gaussian16.

Structures
==========
To generate a 2-D representation of the geometries, use the smiles strings
provided in the .csv file.

Properties
==========
Experimental and computed properties are gathered in the .csv file provided.
Each row contains one molecule. The columns contain the properties, namely:

Miscellanea
-----------
- DonorNo: label of the donor, same as the N for the .xyz files
- PaperLabel: label for the paper
- DOI: digital object identifier for the paper
- DonorName: donor name in the paper
- AcceptorName: acceptor name in the paper
- Notes: particular experimental conditions
- Smiles: smiles string for the Donor

Experimental Properties
-----------------------
- HOMO: experimental HOMO energy for the donor (eV)
- LUMO: experimental LUMO energy for the donor (eV)
- Voc: experimental open circuit voltage value (V)
- Jsc: experimental short circuit current value (mA cm<sup>-2</sup>)
- FF: experimental fill factor value
- PCE: experimental efficiency value

Computed Properties
-------------------
- HOMO_D: DFT/B3LYP/6-31G* HOMO energy for the donor (eV)
- LUMO_D: DFT/B3LYP/6-31G* LUMO energy for the donor (eV)
- LUMO_A: DFT/B3LYP/6-31G* LUMO energy for the acceptor (eV)
- Lambda: DFT/B3LYP/6-31G* reorganisation energy for the electron transfer (eV)
- Scharber_Voc: Scharber open circuit voltage value (V)
- Scharber_Jsc: Scharber short circuit current value (mA cm<sup>-2</sup>)
- Scharber_PCE: Scharber efficiency value
